#### Media Player

[`ExoPlayer`](https://exoplayer.dev)

#### Camera

[`CameraView`](https://natario1.github.io/CameraView/)

#### Compression

[Transcoder](https://github.com/natario1/Transcoder)

#### DateTime

Java 8's `DateTime`[API](https://docs.oracle.com/javase/8/docs/api/index.html?java/time/package-summary.html)

[`ThreeTenABP`](https://github.com/JakeWharton/ThreeTenABP) DEPRECATED

[joda-time-android](https://github.com/dlew/joda-time-android) DEPRECATED

#### Dependency Injection

[Dagger](https://dagger.dev/)

#### Image Processing

[`CircleImageView`](https://github.com/hdodenhof/CircleImageView)

#### Image loading and caching

[Glide](https://bumptech.github.io/glide/)

[Picasso](https://square.github.io/picasso/)

#### Animation

[Lottie](http://airbnb.io/lottie/#/)

#### Data Serialization/Deserialization

[Gson](https://github.com/google/gson)

[Moshi](https://github.com/square/moshi)

#### Cryptography

[Conceal](http://facebook.github.io/conceal/) DEPRECATED

#### Localisation

[LocaleBro](https://localebro.com)
